namespace AuthenApi.Models
{
    public interface ISessionRepository
    {
        Session Add(string username, string password);
        Session Remove(int key);
        Session Find(int key);
    }
}